<?php

session_start();
if($_SESSION["login_user"] != "admin"){
    Header("Location: index.php");
}
require('db.php');
include('header.php');

?>

<script>
    function newDate(days) {
	    return moment().add(days, 'd').toDate();
    }

    $(document).ready(function(){
        $.ajax({
            url: "http://localhost:8004/jobboards/data.php",
            method: "GET",
            type: "json",
            success: function(data){
            var count = data.length / 5;   
            
            var ReactJSData = [];
            var GraphicDesignData = [];
            var AngularJSData = [];
            var SASSData = [];
            var VueJSData = [];

            date = [];
            for(var i = 0; i < count; i++){
                date.push(newDate(i-count+1))
                AngularJSData.push(data[0*count+i].visitcount);
                GraphicDesignData.push(data[1*count+i].visitcount);
                ReactJSData.push(data[2*count+i].visitcount);
                SASSData.push(data[3*count+i].visitcount);
                VueJSData.push(data[4*count+i].visitcount);
            }

            var ctx = document.getElementById("myChart").getContext('2d');
            var timegraph = new Chart(ctx, {
                type: 'line',
                data: {
                    labels: date,
                    datasets: [{
                        label: "AngularJS",
                        borderColor: 'rgba(200, 0, 0, 0.75)',
                        backgroundColor: 'rgba(0, 0, 0, 0)',
                        data: AngularJSData
                    },{
                        label: "Graphic Design",
                        borderColor: 'rgba(200, 50, 100, 0.75)',
                        backgroundColor: 'rgba(0, 0, 0, 0)',
                        data: GraphicDesignData
                    },{
                        label: "ReactJS",
                        borderColor: 'rgba(0, 0, 200, 0.75)',
                        backgroundColor: 'rgba(0, 0, 0, 0)',
                        data: ReactJSData
                    },{
                        label: "SASS",
                        borderColor: 'rgba(255, 105, 180, 0.75)',
                        backgroundColor: 'rgba(0, 0, 0, 0)',
                        data: SASSData
                    }, {
                        label: "VueJS",
                        borderColor: 'rgba(0, 200, 0, 0.75)',
                        backgroundColor: 'rgba(0, 0, 0, 0)',
                        data: VueJSData
                    }]
                }
            });
        }
    });
});
</script>

<div id="chart-container">
	<canvas id="myChart"></canvas>
</div>

<?php include('footer.php') ?>
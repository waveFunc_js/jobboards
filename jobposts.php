<?php
    session_start();
    require('db.php');
    include('header.php');
    $query = $conn->query("select * from posts");
    
        if($query->num_rows > 0){
            ?>
            <div class = "container">
            <table class = "table table-striped table-hover" border = 1>
            <tr>
            <th> Job Title</th>
            <th> Deadline </th>
            </tr>
            <?php
            while($row = $query->fetch_assoc()){
                $datetime1 = date_create($row['deadline']);
                $datetime2 = date_create(date('Y-m-d'));
                $interval = date_diff($datetime2, $datetime1);
                $id = $row['id'];
                $title = $row['title'];
                if($interval->format('%R') == '+' && $row['visible'] == 1){
                    echo "<tr>";
                    echo "<td><a href = 'postdetail.php?id=".$id."'>".$title."</a></td>";
                    echo "<td>".$interval->format('%a days')."</td>";
                    echo "</tr>";
                }
            }
        }
?>
</table>
<?php
include('footer.php');
?>
<?php
    session_start();
    require('db.php');
    include('header.php'); 
    if(!empty($_POST['post-submit'])){
        foreach($_POST as $key=>$value) {
            if(empty($_POST[$key])) {
            $error_message = "All Fields are required";
            break;
            }
        }

        if(!isset($error_message)){
            if($_POST['date'] < date('Y-m-d')){
                $error_message = "Please enter a later date";
            }
        }
        
        if(!isset($error_message)){
            $title = $_POST['title'];
            $description = $_POST['description'];
            $user = $_SESSION['login_user'];
            $deadline = $_POST['date'];
            $date = date('Y-m-d');
            $position = $_POST['position'];
            $insertQuery = "INSERT INTO posts(title, post, username, deadline, published_date, position, visible) VALUES('$title', '$description', '$user', '$deadline', '$date', '$position', 0)";
            $result = $conn->query($insertQuery); 
            if($result){
            $sucess_message = "Post created successfully. Please wait until the admins can verify the post";
            }else{
                $error_message = "Could not create post";
            }
        }
    }
?>

<form name = "postjob" class = "form-signin" action = "" method = "POST">
    <?php if(!empty($error_message)) { ?>	
    <div class="error-message"><?php if(isset($error_message)) echo $error_message; ?></div>
    <?php } else{ ?>
    <div class="success-message"><?php if(isset($sucess_message)) echo $sucess_message; ?></div>

    <?php } ?>
    Title: <input type = "text" class = "form-control" name = "title" value="<?php if(isset($_POST['title'])) echo $_POST['title']; ?>">(limit to 100 characters)<br>
    Post: <textarea rows = "12" class = "form-control" name = "description" ><?php if(isset($_POST['description'])) echo $_POST['description']; ?></textarea>(limit to 2000 characters)<br>
    Position: <select name = "position" class = "form-control">
                    <option value = "ReactJS">ReactJS</option>
                    <option value = "Angular">Angular </option>
                    <option value = "VueJS">VueJS</option>
                    <option value = "SASS">SASS</option>
                    <option value = "Graphic Design">Graphic Design</option>
            </select>
    Deadline: <input type = "date" class = "form-control" name = "date"/>
    <input type = "submit" class = "btn btn-primary" name = "post-submit">
</form>

<?php include('footer.php') ?>
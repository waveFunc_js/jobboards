<?php
    include('header.php');    
    session_start();
    $error = "";
    if (!empty($_POST['login-button'])) {
        $error = "";

        if ($_POST['username'] == "" || $_POST['password'] == "") {
        $error = "Username or Password is invalid";
        }
        else{   require('db.php');
            $username= $_POST['username'];
            $password= $_POST['password'];
            $query = mysqli_query($conn, "select * from account where username='$username'");
            $userResult = mysqli_fetch_assoc($query);
            $rows = mysqli_num_rows($query);
            if ($rows == 1) {
                if(password_verify($password, $userResult['password'])){
                    $_SESSION['id'] = $userResult['id'];
                    $_SESSION['login_user']=$username;
                    header("location: jobposts.php");
                    } else {
                        $error = "Username or Password is invalid";
                    }
                mysqli_close($conn);
            }else{$error = "Username or Password is invalid";}
        }
    }
?>
<script>
    function validate(){
        document.getElementById( "usernameError" ).innerHTML =
        document.getElementById( "passwordError" ).innerHTML = "";
        error = "";
        var name = document.getElementById("username");
        valid = true;
        if(name.value == "" ){

                error = " You Have To Enter a valid username. ";
                document.getElementById( "usernameError" ).innerHTML = error;
                valid = false;
            }

        var password = document.getElementById( "password" );
        if( password.value == ""){
            error = "Enter password";
            document.getElementById( "passwordError" ).innerHTML = error;
            valid = false;
        }

        return valid;
    }
</script>

<div id = "wrapper">
    <form action = "" method = "POST" class = "form-signin" onsubmit = "return validate()">
    <?php if(isset($error)) echo $error; ?>
        <div class = "form-group">
            <label for = "username">Username</label> 
            <input class = "form-control" type = "text" name = "username" id = "username">
            <div id = "usernameError" style = "color:red"></div>
        </div>
        <div class = "form-group">
            <label for = "password">Password</label>
            <input class = "form-control" type = "password" name = "password" id = "password">
            <div id = "passwordError" style = "color:red"></div>
        </div>
        <input class = "btn btn-primary" type = "submit" name = "login-button" >
    </form>
</div>


<?php include('footer.php') ?>
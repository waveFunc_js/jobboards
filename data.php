<?php

    session_start();
    require('db.php');

    $query = "select * from visit order by position, date";
    $queryResult = $conn->query($query);
    
    $data = array();
    foreach ($queryResult as $row){
        $data[] = $row;
    }

    $queryResult->close();
    $conn->close();

    header('Content-type: application/json');
    echo json_encode($data);
?>

